module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
    'vue/setup-compiler-macros': true,
  },
  extends: [
    // 继承eslint推荐的规则集
    'eslint:recommended',
    // vue3基本的规则集
    'plugin:vue/vue3-recommended',
    // 风格约束
    'standard',
    // typescript的规则集
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended', // 新增，必须放在最后面
  ],
  overrides: [],
  // 解析vue文件
  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 'latest',
    // 解析ts语法 解析vue 文件的脚本
    parser: '@typescript-eslint/parser',
    // 配置文件
    // project: ["./tsconfig.json"],
    sourceType: 'module',
  },
  // 添加vue和@typescript-eslint插件，增强eslint的能力
  plugins: ['vue', '@typescript-eslint'],
  rules: {
    semi: 'off',
    'comma-dangle': 'off',
    'vue/multi-word-component-names': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    'space-before-function-paren': 0,
    '@typescript-eslint/ban-types': [
      'error',
      {
        extendDefaults: true,
        types: {
          '{}': false,
        },
      },
    ],
  },
};
